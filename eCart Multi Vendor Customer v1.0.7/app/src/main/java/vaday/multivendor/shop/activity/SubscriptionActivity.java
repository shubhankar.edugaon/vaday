package vaday.multivendor.shop.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.payu.custombrowser.util.e;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import vaday.multivendor.shop.R;
import vaday.multivendor.shop.helper.Constant;
import vaday.multivendor.shop.helper.Session;
import vaday.multivendor.shop.model.SubscriptionModels;

public class SubscriptionActivity extends AppCompatActivity implements PaymentResultListener {

    TextView text, INR_text, perDay_earn, pay_btn;
    String SP_AMOUNT, sp_id, paymentId, userId, PaymentId;
    Session session;
    Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView(R.layout.activity_subscription);

        activity = SubscriptionActivity.this;
        text = findViewById( R.id.full_text);
        INR_text = findViewById( R.id.doller_text);
        perDay_earn = findViewById( R.id.earn_inr_per_day);
        pay_btn = findViewById( R.id.proceed_to_pay);

        pay_btn.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                        try {
                            SharedPreferences sh = getSharedPreferences("MySharedPref", Context.MODE_MULTI_PROCESS);
                            userId = sh.getString("user_id", "");
                            String message = sh.getString( "massage","");
                            if (userId == "" && message == "" ){
                                Intent intent = new Intent( SubscriptionActivity.this, LoginActivity.class);
                                startActivity( intent );
                        }else{
                                startPayment();
                        }
                    }catch (Exception e){
                }
            }
        });
        getvolley();
    }

    private void startPayment(){
        int amount = Math.round(Float.parseFloat(SP_AMOUNT) * 100);
        Checkout checkout = new Checkout();
        checkout.setKeyID("rzp_test_f7V2wpN7AmDeAW");
        checkout.setImage(R.drawable.logo);
        // initialize json object
        JSONObject object = new JSONObject();
        try {
            object.put("name", "Vaday");
            object.put("description", "Test payment");
            object.put("theme.color", "");
            object.put("currency", "INR");
            object.put("amount", amount);
            object.put("prefill.contact", "");
            object.put("prefill.email", "");
            checkout.open(SubscriptionActivity.this, object);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onPaymentSuccess(String s) {
        PaymentId = s;
        Toast.makeText( this,"Payment Successfully"+s,Toast.LENGTH_SHORT).show();
            storePaymentDetails();
    }

    @Override
    public void onPaymentError(int i, String s) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
        super.onPointerCaptureChanged( hasCapture );
    }

    private void getvolley() {
        String url = " ";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray( response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        sp_id = jsonObject.getString("SP_ID");
                        String sp_tt = jsonObject.getString("SP_TT");
                        String SP_DATE = jsonObject.getString("SP_DATE");
                        SP_AMOUNT = jsonObject.getString("SP_AMOUNT");
                        String SP_DAYS = jsonObject.getString("SP_DAYS");
                        String SP_DESC = jsonObject.getString( "SP_DESC");
                        INR_text.setText("₹"+SP_AMOUNT);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText( getApplicationContext(), "", Toast.LENGTH_SHORT ).show();
            }
        }){
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put( "package" ,"1");
                return params;
            }
        };
        stringRequest.setRetryPolicy( new DefaultRetryPolicy(
                20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue =Volley.newRequestQueue(this);
        requestQueue.add( stringRequest);
    }

    private void storePaymentDetails() {
        String url = "https://vaday.in/api-firebase/insert-package.php?package=insert";
        StringRequest stringRequest = new StringRequest( Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Toast.makeText(PrescriptionActivity.this,"successfully",Toast.LENGTH_SHORT).show();
                verifyPackage();
                try {
                    JSONArray jsonArray = new JSONArray( response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText( getApplicationContext(), "Store failed", Toast.LENGTH_SHORT ).show();
            }
        }){
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                        String u_id = userId.toString().trim();
                params.put("transactionid", PaymentId);
                params.put( "packageid" ,sp_id);
                params.put("valid_for","2022-02-27");
                params.put("loginid", u_id);
                return params;
            }
        };
        stringRequest.setRetryPolicy( new DefaultRetryPolicy(
                20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add( stringRequest);
    }

    private void verifyPackage(){
        String url = "https://vaday.in/api-firebase/verify-package.php?package=subscribed";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Toast.makeText(SubscriptionActivity.this,"package verify you can shop!!",Toast.LENGTH_SHORT).show();
                try {
                    JSONArray jsonArray = new JSONArray( response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String PSUB_ID = jsonObject.getString("PSUB_ID");
                        String psubPackageId = jsonObject.getString("PSUB_PACKAGE_IS");

                        SharedPreferences sharedPreferences = getSharedPreferences("MySharedPref",MODE_PRIVATE);
                        SharedPreferences.Editor myEdit = sharedPreferences.edit();
                        myEdit.putString("pusubPackageId", psubPackageId);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText( getApplicationContext(), "Verification failed", Toast.LENGTH_SHORT ).show();
            }
        }){
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                String u_id = userId.toString().trim();
                params.put( "packageid" ,sp_id);
                params.put("loginid", u_id);
                params.put("package","subscribed");
                return params;
            }
        };
        stringRequest.setRetryPolicy( new DefaultRetryPolicy(
                20000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        RequestQueue requestQueue =Volley.newRequestQueue(this);
        requestQueue.add( stringRequest);
    }

}